import discord
from discord.ext import commands

import config
from common import BaseCommon

class EventCog(BaseCommon, commands.Cog):
    """Bot Events"""

    def __init__(self, bot):
        super().__init__(bot)

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        guild_id = guild.id
        await self.dbobj.add_guild(guild_id)
        await self.dbobj.create_users_table(guild_id)

def setup(bot):
    bot.add_cog(EventCog(bot))
