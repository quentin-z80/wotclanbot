import discord
from discord.ext import commands

import zoneinfo

import config
from common import BaseCommon


class AdminCog(BaseCommon, commands.Cog, name="Admin Settings"):
    """Admin Commands"""

    def __init__(self, bot):
        super().__init__(bot)

    @commands.command(name="set_region")
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def set_region(self, ctx, region: str):
        """Sets the guild's region. Can be EU, NA, RU or ASIA"""

        region = region.lower()
        guild_id = ctx.message.guild.id

        if region in config.config["regions"]:
            await self.dbobj.update_guild_data(guild_id=guild_id, region=region)

            await ctx.message.add_reaction("✅")

    @commands.command(name="set_prefix")
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def set_prefix(self, ctx, prefix: str):
        """Sets a custom prefix. Examples: \"!\", \"!wot \", \"<!\""""

        guild_id = ctx.message.guild.id
        await self.dbobj.update_guild_data(guild_id=guild_id, prefix=prefix)

        await ctx.message.add_reaction("✅")

    @commands.command(name="set_clan")
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def set_clan(self, ctx, clan: str):
        """Sets the guilds clan name Example: \"ART-W\""""
        guild_id = ctx.message.guild.id
        region = (await self.dbobj.get_guild_data(guild_id))[3]

        if  region != None:

            data = await self.wot.clans(region=region, search=clan, fields="clan_id", limit=1)

            if data["meta"]["count"] > 0:

                clan_id = data["data"][0]["clan_id"]

                await self.dbobj.update_guild_data(guild_id=guild_id, clan_id=clan_id)

                await ctx.message.add_reaction("✅")

    @commands.command(name="set_schedule")
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def set_battle_chan(self, ctx, channel: discord.TextChannel):
        """Sets channel for scheduled battles to be posted too"""
        guild_id = ctx.message.guild.id

        await self.dbobj.update_guild_data(guild_id, schedule_channel=channel.id)

        await ctx.message.add_reaction("✅")

    @commands.command(name="set_timezone")
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def set_timezone(self, ctx, timezone):
        """Sets the guilds timezone in IANA format. Example: Asia/Dubai"""

        guild_id = ctx.message.guild.id

        if timezone in zoneinfo.available_timezones():
            await self.dbobj.update_guild_data(guild_id, timezone=timezone)
            await ctx.message.add_reaction("✅")

def setup(bot):
    bot.add_cog(AdminCog(bot))
