import discord
from discord.ext import commands

import config
from common import BaseCommon


class StatsCog(BaseCommon, commands.Cog, name="Stat Commands"):
    """Stat Commands"""

    def __init__(self, bot):
        super().__init__(bot)

    
    @commands.group()
    async def stats(self, ctx):
        """Get stats of user. Format: <stats> <(w)ot/(d)iscord> <username> <region, defaults to guild region>"""
        if ctx.invoked_subcommand is None:
            guild_id = ctx.message.guild.id

            region = (await self.dbobj.get_guild_data(guild_id))[3]

            if region in config.config["regions"]:

                wot_id = (await self.dbobj.get_user_data(guild_id, ctx.message.author.id))[1]
                embed = await self.wot_stats_embed(region, wot_id)
                await ctx.send(embed=embed)

    @stats.command(name="w")
    async def stats_wotname(self, ctx, username: str, region: str = ""):
        """lookup stats by WoT username. Format: <username> <region, defaults to guild region>"""

        guild_id = ctx.message.guild.id

        region = region.lower()

        if region not in config.config["regions"]:
            region = (await self.dbobj.get_guild_data(guild_id))[3]

        data = await self.wot.players(region=region, search=username, fields="account_id", limit=1)
        if data["meta"]["count"] != 0:
            userid = data["data"][0]["account_id"]
            embed = await self.wot_stats_embed(region, userid)
            await ctx.send(embed=embed)

    @stats.command(name="d")
    async def stats_discord_wot(self, ctx, username: discord.User):
        """lookup stats by discord username. Format: <username> <region, defaults to guild region>"""

        guild_id = ctx.message.guild.id

        region = (await self.dbobj.get_guild_data(guild_id))[3]

        wot_id = (await self.dbobj.get_user_data(guild_id, username.id))[1]
        embed = await self.wot_stats_embed(region, wot_id)
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(StatsCog(bot))
