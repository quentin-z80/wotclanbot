import discord
from discord.ext import commands

import config
from common import BaseCommon


class GeneralCog(BaseCommon, commands.Cog, name="General Commands"):
    """General Commands"""

    def __init__(self, bot):
        super().__init__(bot)

    @commands.command()
    async def ping(self, ctx):
        """Ping the bot"""
        await ctx.send("Pong!")

    @commands.command(name='set_wotname')
    @commands.guild_only()
    async def set_wot_name(self, ctx, wotname: str):
        """Set your ingame username"""
        guild_id = ctx.message.guild.id
        discord_id = ctx.message.author.id
        region = (await self.dbobj.get_guild_data(guild_id))[3]

        if region != None:

            data = await self.wot.players(region=region, search=wotname, fields="account_id", limit=1, stype="exact")

            if data["meta"]["count"] > 0:
                wot_id = data["data"][0]["account_id"]
                await self.dbobj.update_user_data(guild_id, discord_id, wot_id)
                await ctx.message.add_reaction("✅")

def setup(bot):
    bot.add_cog(GeneralCog(bot))

