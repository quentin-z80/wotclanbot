import aiopg
import os
import json

import psycopg2.errors
from psycopg2 import sql

import config

class Database:

    def __init__(self, pool):
        self.pool = pool

    async def execute_one(self, querry, data=None):
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(querry, data)
                return await cur.fetchone()

    async def execute_all(self, querry, data=None):
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(querry, data)
                return await cur.fetchall()

    async def execute(self, querry, data=None):
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(querry, data)

    async def get_guild_data(self, guild_id):
        querry = "SELECT * FROM discord_guilds WHERE guild_id = %s"
        data = (guild_id,)
        dbdata = await self.execute_one(querry, data)
        if dbdata == None:
            await self.add_guild(guild_id)
            dbdata = await self.execute_one(querry, data)
        return dbdata

    async def get_all_guild_data(self):
        querry = "SELECT * FROM discord_guilds"
        dbdata = await self.execute_all(querry)
        return dbdata

    async def add_guild(self, guild_id):
        querry = "INSERT INTO discord_guilds (guild_id) VALUES (%s)"
        data = (guild_id,)
        await self.execute(querry, data)

    async def update_guild_data(self, guild_id, prefix=None, clan_id=None, region=None, gm_schedule=None, schedule_channel=None, timezone=None, clan_battles=None):

        if gm_schedule == []:
            gm_schedule = None
        elif gm_schedule != None:
            gm_schedule = json.dumps(gm_schedule)

        if clan_battles == []:
            clan_battles = None
        elif clan_battles != None:
            clan_battles = json.dumps(clan_battles)

        querry = """UPDATE discord_guilds 
                 SET prefix = coalesce(%s, discord_guilds.prefix), clan_id = coalesce(%s, discord_guilds.clan_id), region = coalesce(%s, discord_guilds.region), 
                 gm_schedule = coalesce(%s, discord_guilds.gm_schedule), schedule_channel = coalesce(%s, discord_guilds.schedule_channel), 
                 timezone = coalesce(%s, discord_guilds.timezone), clan_battles = coalesce(%s, discord_guilds.clan_battles) WHERE guild_id = %s"""
        data = (prefix, clan_id, region, gm_schedule, schedule_channel, timezone, clan_battles, guild_id)

        await self.execute(querry, data)


    """guild user table methods"""

    async def create_users_table(self, guild_id):
        querry = sql.SQL("CREATE TABLE guild_user_tables.{} (discord_id bigint NOT null, wot_id bigint, PRIMARY KEY (discord_id))").format(sql.Identifier(str(guild_id)))
        await self.execute(querry)

    async def update_user_data(self, guild_id, discord_id, wot_id):
        querry = sql.SQL("""INSERT INTO guild_user_tables.{} (discord_id, wot_id) 
                        VALUES (%s, %s) 
                        ON CONFLICT (discord_id) DO UPDATE 
                        SET wot_id = %s""").format(sql.Identifier(str(guild_id)), sql.Identifier(str(guild_id)))
        data = (discord_id, wot_id, wot_id)
        await self.execute(querry, data)

    async def get_user_data(self, guild_id, discord_id):
        querry = sql.SQL("SELECT * FROM guild_user_tables.{} WHERE discord_id = %s").format(sql.Identifier(str(guild_id)))
        data = (discord_id,)
        dbdata = await self.execute_one(querry, data)
        return dbdata
