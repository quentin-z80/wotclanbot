import discord
from discord.ext import commands

from datetime import datetime
from zoneinfo import ZoneInfo

import config
from database import Database
from wotapi import Wotapi

class Bot(commands.Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.pool = kwargs.pop("pool")

        self.dbobj = Database(self.pool)
        self.wot = Wotapi()

    async def on_ready(self):
        print('\n------')
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------\n')


class BaseCommon:
    def __init__(self, bot):

        self.bot = bot
        self.dbobj = bot.dbobj
        self.wot = bot.wot

    async def winrate_to_color(self, winrate):
        """returns color code in hex for a winrate"""

        # round winrate to int
        winrate = int(round(winrate))

        if winrate < 46:
            return config.config["rating_colors"]["inept"]
        elif winrate == 46:
            return config.config["rating_colors"]["amateur"]
        elif winrate == 47:
            return config.config["rating_colors"]["basic"]
        elif winrate == 48:
            return config.config["rating_colors"]["novice"]
        elif winrate >= 49 and winrate <= 51:
            return config.config["rating_colors"]["intermediate"]
        elif winrate >= 52 and winrate <= 53:
            return config.config["rating_colors"]["adept"]
        elif winrate >= 54 and winrate <= 55:
            return config.config["rating_colors"]["advanced"]
        elif winrate >= 56 and winrate <= 59:
            return config.config["rating_colors"]["expert"]
        elif winrate >= 60 and winrate <= 64:
            return config.config["rating_colors"]["unicum"]
        elif winrate >= 65:
            return config.config["rating_colors"]["super_unicum"]

    async def wotlabs_sig_url(self, region, nickname):
        """returns a url to a users wotlabs forum signature image"""
        wotlabs_url = config.config["wotlabs"]["url"]
        sig_type = config.config["wotlabs"]["sig_type"]

        url = f"{wotlabs_url}/{sig_type}/{region}/{nickname}/signature.png"

        return url

    async def wot_stats_embed(self, region, userid):
        """returns a discord embed object of a users wot stats"""

        fields = "nickname,statistics.all.battles,statistics.all.wins,global_rating,statistics.stronghold_skirmish.battles,statistics.stronghold_defense.battles,statistics.globalmap_absolute.battles,statistics.globalmap_champion.battles,statistics.globalmap_middle.battles"
        extra = "statistics.globalmap_absolute,statistics.globalmap_champion,statistics.globalmap_middle"
        data = await self.wot.player_pdata(region, userid, fields=fields, extra=extra)

        nickname = data["data"][str(userid)]["nickname"]
        battles = data["data"][str(userid)]["statistics"]["all"]["battles"]
        wins = data["data"][str(userid)]["statistics"]["all"]["wins"]
        winrate = (wins / battles) * 100
        global_rating = data["data"][str(userid)]["global_rating"]
        
        stats = data["data"][str(userid)]["statistics"]
        advance_battles = stats["stronghold_defense"]["battles"]
        sh_battles = stats["stronghold_skirmish"]["battles"]
        gm_battles_t6 = stats["globalmap_middle"]["battles"]
        gm_battles_t8 = stats["globalmap_champion"]["battles"]
        gm_battles_t10 = stats["globalmap_absolute"]["battles"]
        gm_battles = gm_battles_t6 + gm_battles_t8 + gm_battles_t10

        embed_color = int(await self.winrate_to_color(winrate))

        embed = discord.Embed(title=f"Stats for {nickname} - Click link for tomato.gg", colour=discord.Colour(embed_color), url=f"https://www.tomato.gg/stats/{region}/{nickname}={userid}")

        stats_url = await self.wotlabs_sig_url(region, nickname)
        embed.set_image(url=stats_url)

        embed.add_field(name="Personal Rating (PR)", value=f"{global_rating}", inline=False)
        embed.add_field(name="Advance Battles", value=f"{advance_battles}", inline=True)
        embed.add_field(name="Stronghold Battles", value=f"{sh_battles}", inline=True)
        embed.add_field(name="Global Map Battles", value=f"{gm_battles}", inline=True)

        return embed

    async def gm_landing_embed(self, region, clan_id, battle, timezone):

        clan_details = (await self.wot.clan_details(region, clan_id=clan_id, fields="emblems.x195,clan_id,tag,name"))["data"]
        owner_details = (await self.wot.clan_details(region, clan_id=battle["owner_clan_id"], fields="emblems.x195,clan_id,tag,name"))["data"]
        clan_win_rate = (await self.wot.clan_ratings(region, clan_id=battle["owner_clan_id"], fields="wins_ratio_avg"))["data"][str(battle["owner_clan_id"])]["wins_ratio_avg"]["value"]
        max_vehicle_level = await self.get_front_tier(region, battle["front_id"])

        if timezone == None:
            timezone = "CET"

        battles_start = datetime.strptime(f"{battle['battles_start_at']}+0000", "%Y-%m-%dT%H:%M:%S%z").astimezone(ZoneInfo(timezone))
        readable_time = battles_start.strftime("%Y-%m-%d %H:%M %Z")

        delta_to_battle = battles_start - datetime.now(ZoneInfo(timezone))
        seconds = delta_to_battle.total_seconds()

        hours = round(seconds // 3600)
        minutes = round((seconds % 3600) // 60)

        if hours > 0 and minutes > 0:
            countdown_text = f"{hours} hours and {minutes} minutes from posting"
        elif hours == 0 and minutes >= 0:
            countdown_text = f"{minutes} minutes from posting"
        elif hours > 0 and minutes == 0:
            countdown_text = f"{hours} hours from posting"
        else:
            return None

        province_text = f"[{battle['province_name']}]({config.config['global_map_url'][region]}{battle['uri']})"
        land_owner_text = f"[{owner_details[str(battle['owner_clan_id'])]['tag']}](https://www.tomato.gg/clan-stats/{region}/{owner_details[str(battle['owner_clan_id'])]['tag']}={battle['owner_clan_id']})"

        embed_color = int(await self.winrate_to_color(clan_win_rate))

        embed = discord.Embed(colour=discord.Colour(embed_color))

        embed.set_thumbnail(url=clan_details[str(clan_id)]["emblems"]["x195"]["portal"])

        embed.set_author(name="New Planned Battle:")

        embed.add_field(name="Battles Start:", value=f"{readable_time} | {countdown_text}", inline=False)

        if battle["battle_type"] == "landing":
            embed.add_field(name="Battle Type:", value="Landing", inline=True)
            embed.add_field(name="Land Owner:", value=land_owner_text, inline=True)
        elif battle["battle_type"] == "land_owner":
            embed.add_field(name="Battle Type:", value="Province Defense", inline=True)
            embed.add_field(name="\u200B", value="\u200B", inline=True)
        elif battle["battle_type"] == "attack":
            embed.add_field(name="Battle Type:", value="Attack", inline=True)
            embed.add_field(name="Land Owner:", value=land_owner_text, inline=True)

        embed.add_field(name="\u200B", value="\u200B", inline=True)

        embed.add_field(name="Map:", value=f"{battle['arena_name']}", inline=True)
        embed.add_field(name="Province:", value=province_text, inline=True)
        embed.add_field(name="Tier:", value=f"{max_vehicle_level}", inline=True)

        return embed

    async def clan_battle_embed(self, region, clan_id, battle, province, timezone):

        clan_details = (await self.wot.clan_details(region, clan_id=clan_id, fields="emblems.x195,clan_id,tag,name"))["data"]
        competitor_details = (await self.wot.clan_details(region, clan_id=battle["competitor_id"], fields="emblems.x195,clan_id,tag,name"))["data"]
        clan_win_rate = (await self.wot.clan_ratings(region, clan_id=battle["competitor_id"], fields="wins_ratio_avg"))["data"][str(battle["competitor_id"])]["wins_ratio_avg"]["value"]
        max_vehicle_level = await self.get_front_tier(region, battle["front_id"])

        if timezone == None:
            timezone = "CET"

        battles_start = datetime.fromtimestamp(battle["time"]).astimezone(ZoneInfo(timezone))
        readable_time = battles_start.strftime("%Y-%m-%d %H:%M %Z")

        delta_to_battle = battles_start - datetime.now(ZoneInfo(timezone))
        seconds = delta_to_battle.total_seconds()

        hours = round(seconds // 3600)
        minutes = round((seconds % 3600) // 60)

        if hours > 0 and minutes > 0:
            countdown_text = f"{hours} hours and {minutes} minutes from posting"
        elif hours == 0 and minutes >= 0:
            countdown_text = f"{minutes} minutes from posting"
        elif hours > 0 and minutes == 0:
            countdown_text = f"{hours} hours from posting"
        else:
            return None

        province_text = f"[{province['province_name']}]({config.config['global_map_url'][region]}{province['uri']})"
        competitor_text = f"[{competitor_details[str(battle['competitor_id'])]['tag']}](https://www.tomato.gg/clan-stats/{region}/{competitor_details[str(battle['competitor_id'])]['tag']}={battle['competitor_id']})"

        embed_color = int(await self.winrate_to_color(clan_win_rate))

        embed = discord.Embed(colour=discord.Colour(embed_color))

        embed.set_thumbnail(url=clan_details[str(clan_id)]["emblems"]["x195"]["portal"])

        embed.set_author(name="New Scheduled Battle:")


        embed.add_field(name="Time:", value=f"{readable_time} | {countdown_text}", inline=False)

        if battle["type"] == "attack" or battle["attack_type"] == "auction":
            embed.add_field(name="Battle Type:", value="Attack", inline=True)
        elif battle["type"] == "defence":
            embed.add_field(name="Battle Type:", value="Defence", inline=True)
        embed.add_field(name="Competitor:", value=competitor_text, inline=True)

        embed.add_field(name="\u200B", value="\u200B", inline=True)

        embed.add_field(name="Map:", value=f"{province['arena_name']}", inline=True)
        embed.add_field(name="Province:", value=province_text, inline=True)
        embed.add_field(name="Tier:", value=f"{max_vehicle_level}", inline=True)

        return embed

    async def get_front_tier(self, region, front_id):
        fronts = (await self.wot.fronts(region, fields="front_id,max_vehicle_level"))["data"]
        for front in fronts:
            if front["front_id"] == front_id:
                return front["max_vehicle_level"]

    async def get_provinces(self, region, front):
        """data from WeeGee api is split into pages, this requests pages until there are none left"""
        data_count = 100
        page = 1
        data = {"region": region, "front_id": front, "data": []}
        while data_count > 0:
            resp_data = await self.wot.provinces(region, front_id=front, page_no=page, fields="attackers,active_battles,arena_name,province_name,status,arena_id,prime_time,battles_start_at,province_id,owner_clan_id,competitors,round_number,front_id,uri")
            data["data"] += resp_data["data"]
            data_count = resp_data["meta"]["count"]
            page += 1

        return data

    async def get_prov_battles(self, clan_id, province_data):
        clan_battles = []
        for front in province_data:
            for provinces in province_data[front]:
                for province in provinces:
                    if province["owner_clan_id"] == clan_id and (province["competitors"] != [] or province["attackers"] != []):
                        province["battle_type"] = "land_owner"
                        clan_battles.append(province)
                    elif clan_id in province["competitors"]:
                        province["battle_type"] = "landing"
                    elif clan_id in province["attackers"]:
                        province["battle_type"] = "attack"
                        clan_battles.append(province)
        return clan_battles

    async def get_new_prov_battles(self, clan_battles, old_battles):
        if old_battles == None:
            old_battles = []
        new_battles = []
        for battle in clan_battles:
            is_new = True
            for old_battle in old_battles:
                # check if battle is the same
                if battle["battles_start_at"][:-3] != old_battle["battles_start_at"][:-3]:
                    continue
                if battle["front_id"] != old_battle["front_id"]:
                    continue
                if battle["owner_clan_id"] != old_battle["owner_clan_id"]:
                    continue
                if battle["arena_id"] != old_battle["arena_id"]:
                    continue
                if battle["province_id"] != old_battle["province_id"]:
                    continue
                if battle["battle_type"] != old_battle["battle_type"]:
                    continue
                is_new = False
            if is_new:
                new_battles.append(battle)

        return new_battles

    async def get_new_clan_battles(self, battles_data, old_battles):
        if old_battles == None:
            old_battles = []
        new_battles = []
        for battle in battles_data:
            is_new = True
            for old_battle in old_battles:
                
                # check if battle is the same
                if battle["front_id"] != old_battle["front_id"]:
                    continue
                if battle["competitor_id"] != old_battle["competitor_id"]:
                    continue
                if int(round(battle["time"], -1)) != int(round(old_battle["time"], -1)):
                    continue
                if battle["vehicle_level"] != old_battle["vehicle_level"]:
                    continue
                if battle["province_id"] != old_battle["province_id"]:
                    continue
                if battle["type"] != old_battle["type"]:
                    continue
                is_new = False
            if is_new:
                new_battles.append(battle)

        return new_battles




